use anyhow::Result as AhResult;

#[derive(Debug)]
pub struct Table<T> {
    slots: Vec<Option<T>>,
}

#[derive(Debug)]
pub struct Slot<'a, T> {
    number: usize,
    handle: &'a mut Option<T>,
}

impl<T> Table<T> {
    pub fn new(n: usize) -> Self {
        let slots = std::iter::repeat_with(|| None).take(n).collect();

        Self { slots }
    }

    pub fn is_empty(&self) -> bool {
        self.slots.iter().all(|slot| slot.is_none())
    }

    pub fn free_slot(&mut self) -> Option<Slot<'_, T>> {
        self.slots
            .iter_mut()
            .enumerate()
            .find(|(_, slot)| slot.is_none())
            .map(|(number, handle)| Slot { number, handle })
    }

    pub fn try_release_slot_if<F: FnMut(&mut T) -> AhResult<bool>>(
        &mut self,
        mut try_pred: F,
    ) -> AhResult<Option<(Slot<'_, T>, T)>> {
        for (n, slot) in self.slots.iter_mut().enumerate() {
            if let Some(v) = slot {
                if !try_pred(v)? {
                    continue;
                }

                let result = slot.take().map(|v| {
                    (
                        Slot {
                            number: n,
                            handle: slot,
                        },
                        v,
                    )
                });

                return Ok(result);
            }
        }

        Ok(None)
    }
}

impl<'a, T> Slot<'a, T> {
    pub fn number(&self) -> usize {
        self.number
    }

    pub fn set(&mut self, v: T) {
        *self.handle = Some(v);
    }
}
