use std::collections::HashSet;
use std::ffi::OsString;
use std::fmt::Write;

use anyhow::{anyhow, bail, Result as AhResult};
use clap::{ArgGroup, Args};
use itertools::Itertools;

use crate::paths;
use crate::paths::Paths;

const TODO_FILE_NAME: &str = "todo.txt";
const TODO_FILE_COMMENT: &str =
    "# One package name per line; empty lines or lines beginning with '#' will be ignored\n";

#[derive(Debug, Args)]
#[clap(group = ArgGroup::new("specs").multiple(true).required(true))]
pub struct TodoSpecs {
    /// Package filter spec to add multiple packages based on e.g. a property
    ///
    /// build_style:<style> - adds all packages with the given build style
    #[arg(short = 'f', long = "filter", group = "specs", value_name = "SPEC")]
    filters: Vec<String>,

    /// Exact package name to add
    #[arg(short = 'p', long = "package", group = "specs", value_name = "PKG")]
    packages: Vec<String>,

    /// Append packages to existing list instead of rewriting it
    #[arg(short, long)]
    append: bool,
}

#[derive(Debug)]
enum Filter {
    BuildStyle(String),
}

#[derive(Debug)]
pub struct Todo {
    paths: Paths,
}

fn uses_build_style(content: &str, style: &str) -> bool {
    content.contains(&format!("build_style={}", style))
}

fn parse_package_line(line: &str) -> Option<String> {
    let line = line.trim();

    if line.is_empty() || line.starts_with('#') {
        return None;
    }

    Some(line.to_string())
}

pub fn exists() -> AhResult<bool> {
    let state_dir = paths::state_path()?;
    let todo_file = state_dir.join(TODO_FILE_NAME);

    todo_file.try_exists().map_err(Into::into)
}

pub fn load() -> AhResult<Vec<String>> {
    let state_dir = paths::state_path()?;
    let todo_file = state_dir.join(TODO_FILE_NAME);

    let content = std::fs::read_to_string(todo_file)?;
    let packages = content.lines().filter_map(parse_package_line).collect();

    Ok(packages)
}

pub fn save(packages: &[OsString]) -> AhResult<()> {
    let state_dir = paths::state_path()?;
    let todo_file = state_dir.join(TODO_FILE_NAME);

    let mut content = String::new();

    write!(&mut content, "{}", TODO_FILE_COMMENT)?;
    for package in packages {
        let package = package
            .to_str()
            .ok_or_else(|| anyhow!("Failed to write package name as UTF-8"))?;
        writeln!(&mut content, "{}", package)?;
    }

    std::fs::write(todo_file, content)?;

    Ok(())
}

pub fn clean() -> AhResult<()> {
    let state_dir = paths::state_path()?;
    let todo_file = state_dir.join(TODO_FILE_NAME);

    eprintln!("Removing all packages from {}", todo_file.display());

    std::fs::write(todo_file, TODO_FILE_COMMENT)?;

    Ok(())
}

impl Todo {
    pub fn new(paths: Paths) -> Self {
        Self { paths }
    }

    fn process_args(&self, specs: &TodoSpecs) -> AhResult<(Vec<Filter>, Vec<String>)> {
        let mut filter_specs = vec![];
        let mut pkg_specs = vec![];

        for filter in &specs.filters {
            let (key, value) = filter
                .split_once(':')
                .ok_or_else(|| anyhow!("Failed to parse template filter {:?}", filter))?;

            let f = match key {
                "build_style" => Filter::BuildStyle(value.to_string()),
                _ => bail!("Unknown filter key: {:?}", key),
            };

            filter_specs.push(f);
        }

        let srcpkgs = self.paths.srcpkgs();

        for package in &specs.packages {
            let mut pkg_dir = srcpkgs.clone();
            pkg_dir.extend([package, "template"]);

            if !pkg_dir.exists() {
                eprintln!("Warning: could not find package {:?}", package);
                continue;
            }

            pkg_specs.push(package.to_string());
        }

        Ok((filter_specs, pkg_specs))
    }

    fn filter_build_style(&self, filters: &[Filter]) -> AhResult<Vec<OsString>> {
        let srcpkgs = self.paths.srcpkgs();
        let entries = std::fs::read_dir(&srcpkgs)?;

        let mut collected_pkgs = vec![];

        for entry in entries {
            let entry = entry?;
            let metadata = entry.metadata()?;

            if metadata.is_symlink() || !metadata.is_dir() {
                continue;
            }

            let template = entry.path().join("template");

            if !template.exists() {
                eprintln!(
                    "Warning: found template directory but no template file for package {:?}",
                    entry.file_name()
                );
                continue;
            }

            let content = std::fs::read_to_string(template)?;

            for filter in filters {
                let matches = match filter {
                    Filter::BuildStyle(style) => uses_build_style(&content, style),
                };

                if matches {
                    collected_pkgs.push(entry.file_name());
                }
            }
        }

        Ok(collected_pkgs)
    }

    pub fn grab(&self, specs: &TodoSpecs) -> AhResult<Vec<OsString>> {
        let mut collected_packages = HashSet::new();
        if specs.append && exists()? {
            let current_packages = load()?;
            collected_packages.extend(current_packages.into_iter().map_into());
        }

        let (filters, packages) = self.process_args(specs)?;

        let filtered = self.filter_build_style(&filters)?;
        collected_packages.extend(filtered);

        collected_packages.extend(packages.into_iter().map_into());

        let result = collected_packages.into_iter().sorted().collect();

        Ok(result)
    }
}
